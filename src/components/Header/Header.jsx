import React from 'react';
import s from './Header.module.css';

const Header = () => {
    return <header className = {s.header}>
        <img src='https://upload.wikimedia.org/wikipedia/commons/thumb/5/5a/Mitsubishi_logo.svg/500px-Mitsubishi_logo.svg.png'></img>
    </header>
}

export default Header;